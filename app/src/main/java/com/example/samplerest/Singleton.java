package com.example.samplerest;

public class Singleton {
    private static JsonPlaceHolderAPI Instanse = null;

    public static JsonPlaceHolderAPI getInstanse(){
        if (Instanse == null){
            Instanse = JsonPlaceHolderAPI.Builder.create();
        }
        return Instanse;
    }
}
