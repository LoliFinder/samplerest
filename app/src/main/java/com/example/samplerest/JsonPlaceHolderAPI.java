package com.example.samplerest;

import com.example.samplerest.Models.Comments;
import com.example.samplerest.Models.Post;

import java.util.List;

import io.reactivex.Observable;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface JsonPlaceHolderAPI {

    @GET("posts")
    Observable<List<Post>> getPosts();

    @GET("posts/{id}")
    Observable<Post> getPosts(@Path("id") int id);

    @GET("posts/{id}/comments")
    Observable<Comments> getPostComments(@Path("id")int id);

     class Builder{
        public static JsonPlaceHolderAPI create(){
            return new Retrofit.Builder()
                    .baseUrl("https://jsonplaceholder.typicode.com/")
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(JsonPlaceHolderAPI.class);
        }
    }
}
