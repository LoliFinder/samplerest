package com.example.samplerest;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.samplerest.Models.Post;

import java.util.List;




public class MainActivity extends AppCompatActivity implements IView{

    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBar =   (ProgressBar) findViewById(R.id.progressBar);
        recyclerView = (RecyclerView) findViewById(R.id.list_posts);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        Presenter presenter = new Presenter(this);
        presenter.getPostsData();


    }

    @Override
    public void showList(List<Post> PostList) {
        myAdapter myAdapter = new myAdapter(this,PostList);
        recyclerView.setAdapter(myAdapter);
    }

    @Override
    public void showError(String error) {
        Toast.makeText(getApplicationContext(),error, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void showEmptyList() {

    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgres() {
        progressBar.setVisibility(View.GONE);
    }


}
