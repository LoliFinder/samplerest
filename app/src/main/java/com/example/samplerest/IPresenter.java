package com.example.samplerest;

public interface IPresenter {
    void getPostsData();
    void onDestroy();
}
