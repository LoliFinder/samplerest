package com.example.samplerest;

import com.example.samplerest.Models.Post;

import java.util.List;

public interface IView {
    void showList(List<Post> PostList);
    void showError(String error);
    void showEmptyList();
    void showProgress();
    void hideProgres();
}