package com.example.samplerest;

import com.example.samplerest.Models.Post;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class Presenter implements IPresenter {
    private IView view;
    public Presenter(IView view){
        this.view = view;
    }


    @Override
    public void getPostsData() {
        view.showProgress();
        Singleton.getInstanse().getPosts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Post>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(List<Post> posts) {
                            if (!posts.isEmpty()){
                            view.showList(posts);
                            } else view.showEmptyList();
                            view.hideProgres();
                    }

                    @Override
                    public void onError(Throwable e) {
                            view.showError(e.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void onDestroy() {
        view = null;
    }
}
